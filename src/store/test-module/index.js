import state from "./state";

export default {
  namespaced: true,
  state: state,
  actions: {},
  getters: {},
  mutations: {}
};
