import { createStore } from "vuex";
import module from "./test-module/index";

export default createStore({
  modules: {
    module
  },
  state: {
  },
  actions: {},
  getters: {},
  mutations: {}
});
