import { createRouter, createWebHistory } from "vue-router";

const Home = () => import("../views/Home.vue");

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

// Guard
// Check if current route confirms the selected step route
router.beforeEach((to, from, next) => {
  next();
});

export default router;
