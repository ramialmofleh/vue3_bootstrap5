# papc

## Project setup

- [Vue 3](https://v3.vuejs.org/guide/introduction.html)
  - [vue-router](https://next.router.vuejs.org/)
  - [Vuex](https://next.vuex.vuejs.org/)
- [Bootstrap 5](https://getbootstrap.com/docs/5.0/getting-started/introduction/) (Beta)
- [Cypress](https://docs.cypress.io/api/api/table-of-contents.html)

## Comands
```
npm run start => Compiles and hot-reloads for development
npm run build => Compiles and minifies for production
npm run lint => Lints and fixes files
npm run format:check => Checks code format and compares with rules
npm run format:fix => Format all code
npm run test:components => Runs tests with cypress
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
